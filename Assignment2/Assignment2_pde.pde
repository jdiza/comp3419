PImage img;
PShape globe;
ArrayList<Ball> balls;
float gravity = 1;
ArrayList<PImage> textures;

void setup() {
  size(800, 800, P3D);
  background(127);
  balls = new ArrayList<Ball>();
  textures = new ArrayList<PImage>();
  for(int i=1; i<12; i++) {
    img = loadImage(((int)random(1,11) + ".jpg"));
    textures.add(img);
  }
}

class Ball {
  float radius;
  PVector location;
  PVector velocity;
  int mass;
  int colour;

  PShape sphere;

  Ball(int x, int y, int r) {
    //this.location = new PVector(x, y, 0);
    this.location = new PVector(x, y, 0);
    this.velocity = new PVector(random(-50,50), random(-50, 50), -10);
    this.mass = 0;
    this.radius = r;
    noFill();
    noStroke();
    this.sphere = createShape(SPHERE, r);
    this.sphere.setTexture(textures.get((int)random(1,11)));
  }

  void display() {
    pushMatrix();
    translate(this.location.x, this.location.y, this.location.z);
    shape(this.sphere, 0, 0);
    popMatrix();
  }
  
  void move() {
    this.location.x += this.velocity.x;
    this.location.y += this.velocity.y;
    this.location.z += this.velocity.z;
    this.velocity.y += gravity;
    this.velocity.y *= 0.97;
    if(this.velocity.y > -5 && this.velocity.y <5 && this.location.y>750) {
     this.velocity.y = 0; 
    }
  }
  void collision() {
    if(this.location.x>800 || this.location.x<0) {
      this.velocity.x = -this.velocity.x;
    }
    if(this.location.y>800 || this.location.y<0) {
      this.velocity.y = -this.velocity.y;
    }
    if(this.location.z<-800 || this.location.z>0) {
      this.velocity.z = -this.velocity.z;
    } //<>//
  }
}

void mouseClicked() {
  Ball newBall = new Ball(mouseX, mouseY, (int)random(15, 150));
  balls.add(newBall);
}
void draw() {
  background(255);
  pushMatrix();
  stroke(0);
  translate(400, 400, 0);
  box(800, 800, 1600);
  popMatrix();
  
  for(Ball ball: balls) {
   ball.collision();
   ball.move();
   ball.display();
  }
}
