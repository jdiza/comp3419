import cv2
import os
import numpy as np
import sys
import tkinter as tk
import tkinter.messagebox as tkmb
from copy import deepcopy
from sklearn.cluster import KMeans
from scipy.spatial import distance
import math


def ask_yn(question):
    response = tkmb.askyesno("Motion Tracking", question)
    return response


def create_folder(folder_name):
    if not os.path.isdir(os.path.join(os.getcwd(), folder_name)):
        os.mkdir(folder_name)
    else:
        print(folder_name + " already exists.")


def save_video_to_frames(video, folder):
    cap = cv2.VideoCapture(video)
    if not cap.isOpened():
        print(str(video) + " from " + str(folder) + " could not be opened.")
        sys.exit(1)
    count = 0
    for count in range(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))):
        ret, frame = cap.read()
        if not ret:
            break
        cv2.imwrite(folder + "/frame%d.tif" % count, frame)
        cv2.imshow(video, frame)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cap.release()
    cv2.destroyAllWindows()
    return int(count)


def show_frames_as_vid(folder):
    count = 0
    frame_count = number_of_files_in_folder(folder)
    while count < frame_count:
        frame = cv2.imread(folder + "/frame%d.tif" % count)
        cv2.imshow(folder, frame)
        count += 1
        if cv2.waitKey(15) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()


def number_of_files_in_folder(folder_path):
    return len(next(os.walk(folder_path))[2])


def produce_binary_frames(source_folder, target_folder, thresholds_bgr):
    for count in range(number_of_files_in_folder(source_folder)):
        original = cv2.imread(source_folder + "/frame%d.tif" % count)
        if original is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break
        binary_img = np.zeros(original.shape[:2])
        for y in range(original.shape[0]):
            for x in range(original.shape[1]):
                blue = original[y, x, 0]
                green = original[y, x, 1]
                red = original[y, x, 2]
                if red > thresholds_bgr[2] and green < thresholds_bgr[1]:
                    binary_img[y, x] = 255
        cv2.imwrite(target_folder + "/frame%d.tif" % count, binary_img)
        cv2.imshow(target_folder, binary_img)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()


def process_dilate_erode(source_folder, target_folder):
    kernel = np.ones((7, 7), np.uint8)
    small_kernel = np.ones((3, 3))
    for count in range(number_of_files_in_folder(source_folder)):
        source = cv2.imread(source_folder + "/frame%d.tif" % count)
        if source is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break

        output = dilateErode2D(source, small_kernel, type="erosion")
        output = dilateErode2D(output, kernel, type="dilate")
        # output = cv2.morphologyEx(source, cv2.MORPH_ERODE, small_kernel)
        # output = cv2.morphologyEx(output, cv2.MORPH_DILATE, kernel)
        cv2.imwrite(target_folder + "/frame%d.tif" % count, output)
        cv2.imshow(target_folder, output)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()


def dilateErode2D(img_in, kernel, type):
    assert type == 'dilate' or type == 'erosion'
    img = img_in[:,:,0]
    final = np.zeros(img_in.shape)
    newimg = np.copy(img)

    kernelSize = kernel.shape[0]
    radius = int(kernelSize / 2)

    h, w  = img.shape
    for x in range (radius, h-radius):
        for y in range (radius, w-radius):
            demo_array = img[x-radius: x+radius+1, y-radius: y+radius+1]
            if type == 'dilate':
                result = np.amax(demo_array * kernel)
            else:
                result = np.amin(demo_array * kernel)
            newimg[x][y] = result

    final[:, :, 0] = newimg
    final[:, :, 1] = newimg
    final[:, :, 2] = newimg

    return final


def convert_binary_to_points(source_img):
    x_points = []
    y_points = []
    for y in range(source_img.shape[0]):
        for x in range(source_img.shape[1]):
            if source_img[y, x] == 255:
                y_points.append(y)
                x_points.append(x)
    pts = [x_points, y_points]
    return np.array(list(zip(x_points, y_points)))


def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)


def process_kmeans(source_folder, target_folder):
    centroids = []
    for count in range(number_of_files_in_folder(source_folder)):
        source = cv2.imread(source_folder + "/frame%d.tif" % count, 0)
        if source is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break
        tracking_pts = KMeans(n_clusters=5).fit(convert_binary_to_points(source))
        centroids.append(tracking_pts.cluster_centers_)
        output = np.zeros(source.shape)
        for point in tracking_pts.cluster_centers_:
            output[int(point[1]), int(point[0])] = 255
            #cv2.circle(output, (int(point[0]), int(point[1])), radius=5, color=255, thickness=-1)
        cv2.imwrite(target_folder + "/frame%d.tif" % count, output)
        cv2.imshow(target_folder, output)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()
    return centroids


def classify_limbs(source_folder, target_folder, organised_colors):
    frame_points_classified = []
    for count in range(number_of_files_in_folder(source_folder)):
        source = cv2.imread(source_folder + "/frame%d.tif" % count, 0)
        if source is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break
        points = []
        for y in range(source.shape[0]):
            for x in range(source.shape[1]):
                if source[y, x] == 255:
                    point = [x, y]
                    points.append(point)

        upper_bound = np.amax(points, axis=0)
        lower_bound = np.amin(points, axis=0)

        top_left = (lower_bound[0], lower_bound[1])
        top_right = (upper_bound[0], lower_bound[1])

        bottom_left = (lower_bound[0], upper_bound[1])
        bottom_right = (upper_bound[0], upper_bound[1])

        middle = (int((upper_bound[0] - lower_bound[0])/2) + lower_bound[0],
                  int((upper_bound[1] - lower_bound[1])/2) + lower_bound[1])
        middle_top = (middle[0], lower_bound[1])
        middle_left = (lower_bound[0], middle[1])
        middle_right = (upper_bound[0], middle[1])
        middle_bottom = (middle[0], upper_bound[1])

        quadrant_points = [top_left, middle_top, top_right,
                           middle_left, middle, middle_right,
                           bottom_left, middle_bottom, bottom_right]
        corners = [top_left, top_right, bottom_left, bottom_right, middle]

        output = np.zeros((source.shape[0], source.shape[1], 3))

        quadrants = draw_quadrants(output, quadrant_points)

        ordered_points = map_limbs(quadrants, corners, points)
        frame_points_classified.append(ordered_points)
        draw_limb_labels(output, ordered_points, organised_colors)

        cv2.imwrite(target_folder + "/frame%d.tif" % count, output)
        cv2.imshow(target_folder, output)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()
    return frame_points_classified


def draw_quadrants(img, quadrant_points):
    cv2.rectangle(img, quadrant_points[0], quadrant_points[4], color=(255, 255, 255))
    cv2.rectangle(img, quadrant_points[1], quadrant_points[5], color=(255, 255, 255))
    cv2.rectangle(img, quadrant_points[3], quadrant_points[7], color=(255, 255, 255))
    cv2.rectangle(img, quadrant_points[4], quadrant_points[8], color=(255, 255, 255))
    quadrants = [[quadrant_points[0], quadrant_points[4]], [quadrant_points[1], quadrant_points[5]],
                 [quadrant_points[3], quadrant_points[7]], [quadrant_points[4], quadrant_points[8]]]
    return quadrants


def map_limbs(quadrants, corners, points):
    ordered_points = []
    for index, quadrant in enumerate(quadrants):
        close_points_index = []
        for point_index, point in enumerate(points):
            if quadrant[0][0] - 2 <= point[0] <= quadrant[1][0] + 2 and quadrant[0][1] - 2 <= point[1] <= quadrant[1][1] + 2:
                close_points_index.append(point_index)
        if len(close_points_index) == 0:
            closest_dist = math.inf
            closest_point_index = 0
            for point_index, point in enumerate(points):
                curr_dist = distance.euclidean(point, corners[index])
                if curr_dist < closest_dist:
                    closest_point_index = point_index
            ordered_points.append(points[closest_point_index])
            points.pop(closest_point_index)
        elif len(close_points_index) > 1:
            closest_dist = math.inf
            closest_point_index = 0
            for close_point_index in close_points_index:
                curr_dist = distance.euclidean(points[close_point_index], corners[index])
                if curr_dist < closest_dist:
                    closest_point_index = close_point_index
                    closest_dist = curr_dist
            ordered_points.append(points[closest_point_index])
            points.pop(closest_point_index)
        else:
            ordered_points.append(points[close_points_index[0]])
            points.pop(close_points_index[0])

    ordered_points.append(points[0])
    return ordered_points


def draw_limb_labels(img, ordered_points, organised_colors):
    labels = ["LH", "RH", "LF", "RF", "M"]
    font = cv2.FONT_HERSHEY_SIMPLEX
    for index, point in enumerate(ordered_points):
        cv2.circle(img, (int(point[0]), int(point[1])), radius=1, color=organised_colors[index], thickness=-1)
        cv2.putText(img, labels.pop(0), (point[0]+5, point[1]+5), font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)


def get_frame_classifications(source_folder, organised_colors, frame_limit):
    ordered_points = []
    for count in range(number_of_files_in_folder(source_folder)):
        source = cv2.imread(source_folder + "/frame%d.tif" % count)
        if source is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break

        for y in range(source.shape[0]):
            for x in range(source.shape[1]):
                if(source[y, x] == (0, 0, 0)).all():
                    continue
                if (source[y, x] == organised_colors[0]).all():
                    LH = (x, y)
                elif (source[y, x] == organised_colors[1]).all():
                    RH = (x, y)
                elif (source[y, x] == organised_colors[2]).all():
                    LF = (x, y)
                elif (source[y, x] == organised_colors[3]).all():
                    RF = (x, y)
                elif (source[y, x] == organised_colors[4]).all():
                    M = (x, y)
        limbs = [LH, RH, LF, RF, M]
        ordered_points.append(limbs)
        print(count)
        if count == frame_limit:
            return ordered_points
    return ordered_points


def fix_classifications(source_folder, target_folder, organised_colors):
    threshold_distance = 50
    over_threshold_dist = 100
    threshold_frames = 949
    frame_points_classified = get_frame_classifications(source_folder, organised_colors, threshold_frames)
    corrected_points_all = []
    corrected_points_all.append(frame_points_classified[0])

    for index, curr_frame in enumerate(frame_points_classified[1:]):
        new_points_frame = []
        for pt_indx, curr_point in enumerate(curr_frame):
            pt_dist = distance.euclidean(curr_point, corrected_points_all[index][pt_indx])
            if over_threshold_dist > pt_dist > threshold_distance:
                new_points_frame.append(corrected_points_all[index][pt_indx])
                print(index + 1)
                print(pt_indx)
                print("distance exceeded")
            else:
                new_points_frame.append(curr_point)
        corrected_points_all.append(new_points_frame)
    for count in range(threshold_frames):
        source = cv2.imread(source_folder + "/frame%d.tif" % count)
        if source is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break
        output = np.zeros(source.shape)
        draw_limb_labels(output, corrected_points_all[count], organised_colors)
        cv2.imwrite(target_folder + "/frame%d.jpg" % count, output)
        cv2.imshow(target_folder, output)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()
    with open('ExportedCoords.txt', 'w+') as file:
        for frame in corrected_points_all:
            file.write(str(frame[0][0]) + " " + str(frame[0][1]) + " ")
            file.write(str(frame[1][0]) + " " + str(frame[1][1]) + " ")
            file.write(str(frame[2][0]) + " " + str(frame[2][1]) + " ")
            file.write(str(frame[3][0]) + " " + str(frame[3][1]) + " ")
            file.write(str(frame[4][0]) + " " + str(frame[4][1]) + "\n")
    return corrected_points_all


def replace_bg_monkey(source_folder, target_folder, organised_colors, img_folder):
    threshold_frames = 5
    frame_points_classified = get_frame_classifications(source_folder, organised_colors, threshold_frames)
    for count in range(threshold_frames):
        source = cv2.imread(source_folder + "/frame%d.tif" % count)
        if source is None:
            print("No frame found " + source_folder + "/frame%d.tif" % count)
            break

        output = add_happy(source, img_folder, frame_points_classified[count])
        cv2.imwrite(target_folder + "/frame%d.tif" % count, output)
        cv2.imshow(target_folder, output)
        if cv2.waitKey(30) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()


def add_happy(source, img_folder, points):
    output = np.zeros(source.shape)
    for index, point in enumerate(points):
        if index == 0:
            img = cv2.imread(img_folder + "/leftHand.png", )
        if index == 1:
            img = cv2.imread(img_folder + "/rightHand.png",)
        if index == 2:
            img = cv2.imread(img_folder + "/leftFoot.png", -1)
        if index == 3:
            img = cv2.imread(img_folder + "/rightFoot.png", -1)
        if index == 4:
            img = cv2.imread(img_folder + "/happy_compress.png", -1)
        x_offset = point[0] - int(img.shape[0]/2)
        y_offset = point[1] - int(img.shape[1]/2)
        output[y_offset:y_offset + img.shape[0], x_offset:x_offset + img.shape[1]] = img
    return output

