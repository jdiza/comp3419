import ddf.minim.*;
Minim minim;
AudioPlayer aye;
AudioPlayer wow;

int frame_index;
float[] RED;
float[] BLUE;
float[] GREEN;
float[] PINK;
float[] TEAL;
ArrayList<ArrayList<Integer>> frameCoords;
ArrayList<Fish> fishList;
int fishCount;
Carla carla;

PImage fishimg;
PImage heart;

void setup() {
  frame_index = 0;
  fishCount = 0;
  size(568, 320);
  background(0);
  fishimg = loadImage("images/fish.png");
  
  minim = new Minim(this);
  RED = new float[]{255, 0, 0};
  BLUE = new float[]{0, 0, 255};
  GREEN = new float[]{0, 255, 0};
  PINK = new float[]{255, 0, 255};
  TEAL = new float[]{0, 255, 255};
  frameCoords = new ArrayList<ArrayList<Integer>>();
  fishList = new ArrayList<Fish>();
  String[] lines = loadStrings("ExportedCoords.txt");
  for (int i = 0 ; i < lines.length; i++) {
    ArrayList<Integer> coords = new ArrayList<Integer>();
    String[] strCoords = lines[i].split(" ", 0);
    for(String s: strCoords) {
     coords.add(Integer.parseInt(s)); 
    }
    frameCoords.add(coords);
  }
  carla = new Carla(80, 80);
  wow = minim.loadFile("sounds/wow.mp3");
}


void draw() {
  if(frame_index == 948) {
   exit(); 
  }
  background(0);
  PImage bg = loadImage("images/background.jpg");
  PImage leftHand = loadImage("images/leftHand.png");
  PImage rightHand = loadImage("images/rightHand.png");
  PImage leftFoot = loadImage("images/leftFoot.png");
  PImage rightFoot = loadImage("images/rightFoot.png");
  PImage body = loadImage("images/happy_compress.png"); 
  heart = loadImage("images/heart.png");
  image(bg, 0, 0);
  if(frame_index % 40 == 0) {
    for(int i = 0; i<3; i++) {
     int x = (int)random(fishimg.width, 568-fishimg.width);
     int y = (int)random(fishimg.height, 320-fishimg.height);
     Fish f = new Fish(x, y);
     fishList.add(f);
    }
  }
  for(Fish f: fishList) {
   if(f.checkCollision() == true && f.capturable == true) {
     f.capture();
     aye = minim.loadFile("sounds/aye.mp3");
     aye.play();
     fishCount += 1;
   }
  }
  for(Fish f: fishList) {
   f.display(); 
  }
  if(fishCount > 0) {
    textSize(32);
    text(Integer.toString(fishCount), 30, 30); 
    fill(0, 0, 0);
  }
  
  carla.display();
  strokeWeight(12);
  stroke(79, 145, 195);
  line(frameCoords.get(frame_index).get(8) - body.width/4, frameCoords.get(frame_index).get(9) + body.height/4, 
        frameCoords.get(frame_index).get(0), frameCoords.get(frame_index).get(1) +15);
  line(frameCoords.get(frame_index).get(8), frameCoords.get(frame_index).get(9) + body.height/4, 
        frameCoords.get(frame_index).get(2), frameCoords.get(frame_index).get(3) + 15);
  strokeWeight(15);
  line(frameCoords.get(frame_index).get(8) - 38, frameCoords.get(frame_index).get(9) + 50, 
        frameCoords.get(frame_index).get(4) + 10, frameCoords.get(frame_index).get(5)+ 10);
  line(frameCoords.get(frame_index).get(8) - 10, frameCoords.get(frame_index).get(9) + 60, 
        frameCoords.get(frame_index).get(6) - 5, frameCoords.get(frame_index).get(7) + 10);
  image(leftHand, frameCoords.get(frame_index).get(0) - leftHand.width/2, frameCoords.get(frame_index).get(1) - leftHand.height/2);
  image(rightHand, frameCoords.get(frame_index).get(2) - rightHand.width/2, frameCoords.get(frame_index).get(3) - rightHand.height/2);
  image(leftFoot, frameCoords.get(frame_index).get(4) - leftFoot.width/2, frameCoords.get(frame_index).get(5) - leftFoot.height/2);
  image(rightFoot, frameCoords.get(frame_index).get(6) - rightFoot.width/2, frameCoords.get(frame_index).get(7) - rightFoot.height/2);
  image(body, frameCoords.get(frame_index).get(8) - body.width/2, frameCoords.get(frame_index).get(9) - body.height/2);
  
  float dist = carla.checkDistance();
  int heartSize = (int)(100 - dist/2);
  if(heartSize < 10) {
    heartSize = 10;
  }
  if(dist < 100) {
    wow.play();
  }
  if(dist > 200) {
    wow.rewind();
  }
  heart.resize(0, heartSize);
  image(heart, frameCoords.get(frame_index).get(8) - heart.width/2, frameCoords.get(frame_index).get(9) - heart.height/2 + 40);
  
  saveFrame("step7-replace_bg_monkey/frame" + frame_index + ".jpg");
  frame_index += 1;
}



class Fish {
  PVector location;
  PVector velocity;
  PImage img;
  boolean capturable;
  Fish(int x, int y) {
    this.img = loadImage("images/fish.png");
    this.location = new PVector(x, y);
    this.velocity = new PVector(random(-5,5), random(1, 5));
    this.capturable = true;
  }
  
  void display() {
    image(img, this.location.x - img.width/2, this.location.y - img.height/2);
    this.location.x += this.velocity.x;
    this.location.y += this.velocity.y;
  }
  
  PImage getImg() {
   return img; 
  }
  
  boolean checkCollision() {
    
    if(dist(location.x, location.y, frameCoords.get(frame_index).get(0), frameCoords.get(frame_index).get(1)) < 40) {
      return true;
    }
    else if(dist(location.x, location.y, frameCoords.get(frame_index).get(2), frameCoords.get(frame_index).get(3)) < 40) {
      return true;
    }
    else if(dist(location.x, location.y, frameCoords.get(frame_index).get(4), frameCoords.get(frame_index).get(5)) < 40) {
      return true;
    }
    else if(dist(location.x, location.y, frameCoords.get(frame_index).get(6), frameCoords.get(frame_index).get(7)) < 40) {
      return true;
    }
    else if(dist(location.x, location.y, frameCoords.get(frame_index).get(8), frameCoords.get(frame_index).get(9)) < 80) {
      return true;
    }
    else {
     return false; 
    }
  }
  
  void capture() {
   this.location.x = img.width - 15;
   this.location.y = img.height - 10;
   this.velocity.x = 0;
   this.velocity.y = 0;
   this.capturable = false;
  }
}

class Carla {
  PVector location;
  PVector velocity;
  PImage img;
  Carla(int x, int y) {
    this.img = loadImage("images/carla.png");
    this.location = new PVector(x, y);
    this.velocity = new PVector(random(-5,5), random(1, 5));
  }
  
  void display() {
    image(img, this.location.x - img.width/2, this.location.y - img.height/2);
    this.location.x += this.velocity.x;
    this.location.y += this.velocity.y;
    if(location.x < 0 || location.x > 568) {
      this.velocity.x = -this.velocity.x;
    }
    if(location.y < 0 || location.y > 320) {
      this.velocity.y = -this.velocity.y;
    }
    
  }
  
  float checkDistance() {
    return dist(location.x, location.y, frameCoords.get(frame_index).get(8), frameCoords.get(frame_index).get(9));
  }
}
