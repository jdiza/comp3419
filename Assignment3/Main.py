import Assignment3Modules as a3

RED_THRESHOLD = 165
GREEN_THRESHOLD = 120
BLUE_THRESHOLD = 20
THRESHOLDS = [BLUE_THRESHOLD, GREEN_THRESHOLD, RED_THRESHOLD]
RED = (0, 0, 255)
BLUE = (255, 0, 0)
GREEN = (0, 255, 0)
PINK = (255, 0, 255)
TEAL = (255, 255, 0)
organised_colors = [RED, BLUE, GREEN, PINK, TEAL]
frame_count = 0
centroids = []

if a3.ask_yn("Create folders?"):
    a3.create_folder("step1-original")
    a3.create_folder("step2-binary")
    a3.create_folder("step3-dilated_eroded")
    a3.create_folder("step4-kmeans")
    a3.create_folder("step5-limbs_classified")
    a3.create_folder("step6-correct_limbs")
    a3.create_folder("step7-replace_bg_monkey")

if a3.ask_yn("Extract from source video to folder?"):
    frame_count = a3.save_video_to_frames("monkey (option1).mov", "step1-original")

if a3.ask_yn("Produce binary frames?"):
    a3.produce_binary_frames("step1-original", "step2-binary", THRESHOLDS)

if a3.ask_yn("Dilate and erode?"):
    a3.process_dilate_erode("step2-binary", "step3-dilated_eroded")

if a3.ask_yn("Run Kmeans on binary?"):
    centroids = a3.process_kmeans("step3-dilated_eroded", "step4-kmeans")

if a3.ask_yn("Classify cluster centroids?"):
    frame_points_classified = a3.classify_limbs("step4-kmeans", "step5-limbs_classified", organised_colors)

if a3.ask_yn("Correct limb classifier?"):
    frame_points_classified = a3.fix_classifications("step5-limbs_classified", "step6-correct_limbs", organised_colors)

#not working, switched to processing to replace marionette
# if a3.ask_yn("Replace background and marionette?"):
#     frame_points_classified = a3.replace_bg_monkey("step6-correct_limbs", "step7-replace_bg_monkey", organised_colors, "images")

# a3.show_frames_as_vid("step6-correct_limbs")