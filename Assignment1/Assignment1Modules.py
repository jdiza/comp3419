import cv2
import os
import sys
import numpy as np
import tkinter as tk
import tkinter.messagebox as tkmb
from tqdm import tqdm
from Frame import Frame, Block
import math
GRID_BLOCK_SIZE = 9
FRAME_WIDTH = 720
FRAME_HEIGHT = 576

def ask_yn(question):
    response = tkmb.askyesno("Lab Submission 1", question)
    return response


def create_folder(folder_name):
    if not os.path.isdir(os.path.join(os.getcwd(), folder_name)):
        os.mkdir(folder_name)
    else:
        print(folder_name + " already exists.")


# Takes video file name and saves frames to intended folder path
# returns: number of frames of video
# video : String of video file name
# folder : String of folder path
def save_video_to_frames(video):
    cap = cv2.VideoCapture(video)
    if not cap.isOpened():
        print(str(video) + " could not be opened.")
        sys.exit(1)
    frames = []
    while(1):
        ret, frame = cap.read()
        if not ret:
            break
        total_blocks = []
        block_rows = np.array(np.vsplit(frame, int(FRAME_HEIGHT/GRID_BLOCK_SIZE)))
        for row_index, row in enumerate(block_rows):
            blocks_row = np.array(np.hsplit(row, int(FRAME_WIDTH / GRID_BLOCK_SIZE)))
            blocks_row_obj = []
            for col, block in enumerate(blocks_row):
                new_block = Block(block, col, row_index)
                blocks_row_obj.append(new_block)
            total_blocks.append(blocks_row_obj)
        frames.append(Frame(frame, np.array(total_blocks)))
        cv2.imshow(video, frame)
        if cv2.waitKey(1) & 0xff == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()
    return frames


def draw_points(frame):
    for block_row_index, block_row in enumerate(frame.blocks):
        for block_column_index, block in enumerate(block_row):
            magnitude = np.linalg.norm(block.displacement)
            # blockx, blocky = block.centre_coord
            # blockx = blockx + (block.pixels.shape[0] * block_column_index)
            # blocky = blocky + (block.pixels.shape[0] * block_row_index)
            # cv2.circle(frame.pixels, (int(blockx), int(blocky)), int(magnitude), (0, 0, 255), -1)
            if min(block.ssds) > 150: # and magnitude > 3:
                blockx, blocky = block.centre_coord
                blockx = blockx + (block.pixels.shape[0] * block_column_index)
                blocky = blocky + (block.pixels.shape[0] * block_row_index)
                cv2.circle(frame.pixels, (int(blockx), int(blocky)), int(magnitude), (0, 0, 255), -1)
                # cv2.line(frame.pixels, (blockx, blocky), (matched_blockx, matched_blocky), (0, 0, 255))
    return frame


def pad_grid_block(vector, pad_width, iaxis, kwargs):
    pad_value = None
    vector[:pad_width[0]] = pad_value
    vector[-pad_width[1]:] = pad_value
    return vector


def compute_displacement(current_frame, next_frame):
    padded_next_frame_blocks = np.pad(next_frame.blocks, 5, pad_grid_block)
    for curr_block_row_index, curr_block_row in enumerate(current_frame.blocks):
        for curr_block_column_index, curr_block in enumerate(curr_block_row):
            search_area = padded_next_frame_blocks[curr_block_row_index:curr_block_row_index+10, curr_block_column_index:curr_block_column_index+10]
            for next_block_row_index, next_block_row in enumerate(search_area):
                for next_block_column_index, next_block in enumerate(next_block_row):
                    if next_block is None:
                        continue
                    ssd = curr_block.compute_ssd(next_block)
                    if ssd <= min(curr_block.ssds):
                        curr_block.displacement = [next_block.y - curr_block.y, next_block.x - curr_block.x]
    return current_frame