import Assignment1Modules as a1
import cv2
import numpy as np
from tqdm import tqdm
frames = []
if a1.ask_yn("Extract frames from source video?"):
    frames = a1.save_video_to_frames("monkey.avi")  #752
    print(frames[0].blocks[0, 0].pixels[0, 0]) #BGR
    current_frame = frames[0]
    next_frame = frames[1]
    #a1.compute_displacement(current_frame, next_frame)

if a1.ask_yn("Create Motion Detection Frames?"):
    count = 0
    for index, frame in enumerate(tqdm(frames[:-1])):
        new_frame = a1.draw_points(a1.compute_displacement(frame, frames[index+1]))
        cv2.imwrite("target/%d.tif" % count, new_frame.pixels)
        count += 1



