import math
import numpy as np


class Frame:
    def __init__(self, pixels, blocks):
        self.pixels = pixels
        self.blocks = blocks


class Block:
    def __init__(self, pixel_array, x, y):
        self.x = x
        self.y = y
        self.pixels = pixel_array
        self.centre_pixel = pixel_array[int(pixel_array.shape[0]/2), int(pixel_array.shape[1]/2)]
        self.centre_coord = (int(pixel_array.shape[0]/2), int(pixel_array.shape[1]/2))
        self.ssds = []
        self.displacement = 0

    def compute_ssd(self, other_block):
        block1_pixels = self.pixels.astype(np.int16)
        block2_pixles = other_block.pixels.astype(np.int16)
        ssd = np.subtract(block1_pixels, block2_pixles)
        ssd = ssd.flatten().sum()
        ssd = np.power(ssd, 2)
        ssd = math.sqrt(ssd)
        self.ssds.append(ssd)
        return ssd